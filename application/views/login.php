<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta name="description" content="Web-Based Business Application">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="<?=base_url("media")?>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url("media")?>/css/reset.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url("media")?>/css/color.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url("media")?>/css/form.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url("media")?>/css/login.css">
	<script type="text/javascript" src="<?=base_url("media")?>/js/jquery.js"></script>
	<script type="text/javascript">
	$(function() {
		$("#form-login").submit(function(e) {
			e.preventDefault();
			if ($("#login").val() != "" && $("#pass").val() != "") {
				var username = $("#login").val(),
					password = $("#pass").val();
				$.post('<?=base_url()?>index.php/login/do_login', { username: username, password: password }, function(data) {
					if (data == "refresh") window.location.assign("index.php/home");
				});
			}
		});
	});
	</script>
</head>
<body>
	<div id="container">
<!-- 
		<hgroup id="login-title" class="large-margin-bottom">
			<h1><img style="width: 200px; height: 120px;" src="<?php echo base_url("media");?>/img/logo/<?php echo $this->session->userdata("company_logo");?>" alt="<?php echo $this->session->userdata("application_name");?>"></h1>
			<h5><?php echo $this->session->userdata("company_name");?></h5>
		</hgroup>
-->
		<form method="post" action="" id="form-login">
			<ul class="inputs black-input large">
				<!-- The autocomplete="off" attributes is the only way to prevent webkit browsers from filling the inputs with yellow -->
				<li><span class="icon-user mid-margin-right"></span><input type="text" name="login" id="login" value="" class="input-unstyled" placeholder="Login" autocomplete="off"></li>
				<li><span class="icon-lock mid-margin-right"></span><input type="password" name="pass" id="pass" value="" class="input-unstyled" placeholder="Password" autocomplete="off"></li>
			</ul>

			<button type="submit" class="button glossy full-width huge">Login</button>
		</form>

	</div>
</body>
</html>