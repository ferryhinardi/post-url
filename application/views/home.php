<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript" src="<?=base_url("media")?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?=base_url()?>nodejs/node_modules/socket.io/node_modules/socket.io-client/socket.io.js"></script>
	<!-- <script type="text/javascript" src="<?=base_url("media")?>/js/socket.js"></script>-->
	<!-- <script type="text/javascript" src="<?=base_url("media")?>/js/main.js"></script>-->
	<script type="text/javascript">
	$(function() {
		$("#btnReload").click(function(e) {
			e.preventDefault();
			$.post("<?=base_url()?>index.php/home/unsetSession", function(data) {
				window.location.assign("<?=base_url()?>");
			});
		});
	})
	</script>
</head>
<body>
	<div style="position: absolute; right: 10px;">
		Welcome, <b>
		<?php 
			echo (isset($username) ? $username : "");
		?>
		</b>
	</div>
	<?php 
		$data = (isset($data) ? $data : array());
		if (count($data) > 0) {
	?>
	<table width="500px" style="text-align:center;">
		<thead>
			<tr>
				<th>UID</th>
				<th>Clockin</th>
			</tr>
		</thead>
		<tbody>
	<?php
		foreach ($data as $key => $value) {
			# code...
	?>
			<tr>
				<td><?=$value["uid"]?></td>
				<td><?=$value["clockin"]?></td>
			</tr>
	<?php }	?>
		</tbody>
	</table>
	<?php } ?>
	<button id="btnReload">Logout (Reset)</button>

	<pre>
		Change Url to home/postService with parameter: uid & clockin
		<b>Example</b>
		http://localhost/[Project_Name]/index.php/home/postService?uid=1&clockin=12
	</pre>
</body>
</html>