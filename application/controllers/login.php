<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()
	{
		$this->load->view('login');
	}

	public function do_login() 
	{
		$post = $this->input->post();
		$this->session->set_userdata('username', $post['username']);
		$this->output->set_output("refresh");
	}
}