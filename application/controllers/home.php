<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$this->load->model('content');
		/*if ($this->session->userdata("data") != NULL) {
			$data = $this->session->userdata("data");
		}
		else {
			$data = array();
		}*/
		if ($this->session->userdata("username") != NULL) {
			$username = $this->session->userdata("username");
		}
		else {
			$username = "";
		}
		$data = $this->content->getContent();
		$this->load->view('home', array('data' => $data, 'username' => $username));
	}

	public function postService() 
	{
		$this->load->model('content');
		$uid = $this->input->get('uid');
		$clockin = $this->input->get('clockin');
		/*if ($this->session->userdata("data") != NULL) {
			$data = $this->session->userdata("data");
		}
		else {
			$data = array();
		}
		*/
		if ($this->session->userdata("username") != NULL) {
			$username = $this->session->userdata("username");
		}
		else {
			$username = "";
		}
		// $post = array('uid' => $uid, 'clockin' => $clockin);
		// array_push($data, $post);
		// $this->session->set_userdata("data", $data);
		$this->content->insertContent($uid, $clockin);
		$data = $this->content->getContent();
		$this->load->view('home', array('data' => $data, 'username' => $username));
	}

	public function getContent()
	{
		# code...
		$this->load->model('content');
		$data = $this->content->getContent();
		$this->output->set_output(json_encode($data));
	}

	public function unsetSession()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}