<?php
class Content extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getContent() {
		$query = $this->db->get('mapping');
		$result = $query->result_array();
		return $result;
	}

	function insertContent($uid, $clockin) {
		$data = array(
			'uid' => $uid,
			'clockin' => $clockin
		);

		$this->db->insert('mapping', $data);
		return true;
	}
}